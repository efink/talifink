/**
 * @fileoverview Constatns
 * @author eyal.fink@gmail.com (Eyal Fink)
 */



var fink = fink || {};
fink.consts = fink.consts || {};

fink.consts.IMAGE_RATIO = 476.0/868.0;


fink.consts.CARDS =
  [{name: 'myvision',
    color: '#dbf039',
    image: 'static/myvision_image.jpg',
    title: 'my vision',
    linkText: 'החזון שלי',
    content: 'htmls/myvision.html'},
    {name: 'aboutme',
    color: '#afc6f2',
    image: 'static/imtali_image.jpg',
    title: 'i am tali',
    linkText: 'אני טלי',
    content: 'htmls/aboutme.html'},
   {name: 'faq',
    color: '#eaa69f',
    image: 'static/adleriani_image2.jpg',
    linkText: 'מהו אימון אדלריאני',
    title: 'what is what...',
    content: 'htmls/faq.html'},
   {name: 'services',
    color: '#aff1b3',
    image: 'static/maslulim_image.jpg',
    linkText: 'מה עושים באימון...',
    title: 'training...',
    content: 'htmls/services.html'},
   {name: 'testimonies',
    color: '#ed5645',
    image: 'static/talking_image.jpg',
    linkText: 'מתאמנים מספרים...',
    title: 'people talking...',
    content: [
      'htmls/ayelet.html',
      'htmls/korach.html',
      'htmls/adi.html',
      'htmls/noy.html',
      'htmls/lili.html',
      'htmls/keren.html',
      'htmls/naama.html',
      'htmls/merav.html'
      ]},
   {name: 'contact',
    color: '#d6d5d3',
    image: 'static/contact_image.jpg',
    linkText: 'צור קשר',
    title: 'contact me',
    content: 'htmls/contact.html'}];
