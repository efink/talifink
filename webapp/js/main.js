/**
 * @fileoverview The main entry point.
 * The init function is called from the top level HTML page after page load.
 * It create the top level DOM and initiale all the objects which controls
 * the different parts of the page.
 * @author eyal.fink@gmail.com (Eyal Fink)
 */


var fink = fink || {};

fink.App = fink.App || {};

/*
 * Constructor
 */
fink.App = function() {
};

fink.App.prototype.init = function() {

  $('#fink-spinner')
    .ajaxStart(function() {
        $('body').addClass('loading');
    })
    .ajaxStop(function() {
        $('body').removeClass('loading')
    });

  fink.Themes.deployThemesCss();

  this.viewport = $('#viewport');
  this.sidePanelDiv = $('<div id="fink-sidepanel">').appendTo(this.viewport);
  this.cardDiv = $('<div id="fink-card">').appendTo(this.viewport);
  this.card = new fink.Card(this.cardDiv);
  this.sidePanel = new fink.SidePanel(this.sidePanelDiv, this.card);

  this.footerDiv = $('<div class="g-section" id="fink-footer">').appendTo(
    $('body'));
  this.footer = new fink.Footer(this.footerDiv);

  this.resize();

  $(window).resize($.proxy(this.resize, this));

};


fink.App.prototype.redraw = function() {
};


/*
 * http://andylangton.co.uk/articles/javascript/get-viewport-size-javascript/
 */
fink.App.viewportSize = function() {

  var useJquery = true;
  var width =  $(window).width();
  var height = $(window).height();
  if (!useJquery) {
    var e = window;
    var a = 'inner';
    if (!('innerWidth' in window)) {
      a = 'client';
      e = document.documentElement || document.body;
    }
    var width = e[a+'Width'];
    var height = e[a+'Height'];
  }
  console.log('widow width: ' + width + ', height:' + height);
  return {width: width, height: height};
};

fink.App.prototype.resize = function() {
  this.card.resize();
  this.sidePanel.resize();
  this.footer.resize();
};

fink.App.prototype.getCardAndContent = function() {
  var cardIndex = -1;
  var selectedContent = null;
  if(window.location.hash) {
    var hash = window.location.hash.substring(1);
    var parts = hash.split(':');
    selectedContent = parts.length > 1 ? Number(parts[1]) : 0;
    for (var i=0; i<fink.consts.CARDS.length; i++) {
      if (fink.consts.CARDS[i].name == parts[0]) {
        cardIndex = i;
        break;
      }
    }
  }
  if (cardIndex >= 0)
    return {index: cardIndex,
            info: fink.consts.CARDS[cardIndex],
            selectedContent: selectedContent};
  return {index: 0,
          info: fink.consts.CARDS[0]};
};

fink.App.prototype.trackPageview = function() {
  var path = location.pathname + location.search + location.hash;
  ga('send', 'pageview', path);
};

fink.App.prototype.urlChangeHandler = function() {
  var currentCard = this.getCardAndContent();
  if (!this.currentCard || currentCard.index != this.currentCard.index) {
    this.sidePanel.selecetCard(currentCard.info,
                               currentCard.index,
                               currentCard.selectedContent);
  } else {
    this.card.setCurrentContent(currentCard.selectedContent);
  }
  this.currentCard = currentCard;
  this.trackPageview();
};


fink.init = function() {
  fink.App.instance = new fink.App();
  fink.App.instance.init();
  $(window).hashchange($.proxy(fink.App.instance.urlChangeHandler,
                               fink.App.instance));
};
