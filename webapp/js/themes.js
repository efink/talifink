/**
 * @fileoverview Create the Themes css class and rules
 *
 * @author eyal.fink@gmail.com (Eyal Fink)
 */



var fink = fink || {};
fink.Themes = {};

fink.Themes.cssThemeString = function(cardInfo) {

  var name = cardInfo.name;
  var cssClass = ".fink-theme-"+name;
  var cssStrings = [];
  // cssStrings.push(cssClass + " #fink-card-image {" +
  //                   "background: url("+cardInfo.image+");" +
  //                   "background-size: cover;}");

  // cssStrings.push(cssClass + " .fink-small-image #fink-card-image {" +
  //                   "background: url(" +
  //                   cardInfo.image.replace('.jpg', '_half.jpg') +
  //                   ");" +
  //                   "background-size: cover;}");

  cssStrings.push(cssClass + " .fink-sisepanel-nav-box-selected," +
                  "#fink-sisepanel-nav-box-"+ name +":hover {" +
                    "background-color: "+ cardInfo.color +";}");
  cssStrings.push(cssClass + " #fink-card-content h2 {" +
                   "color: " + cardInfo.color + ";" +
                   "font-size: 140%;}");

  cssStrings.push(cssClass + " #fink-card-content hr {" +
                  "border-top-color: "+ cardInfo.color +";}");

  cssStrings.push(cssClass + " #fink-card-content li:before {" +
                  "color: "+ cardInfo.color +";}");

  return cssStrings.join("");
};

fink.Themes.getThemesCss = function() {
  var css = '';
  $(fink.consts.CARDS).each(function(i, cardInfo){
    css += fink.Themes.cssThemeString(cardInfo);
  })
  return css;
};

fink.Themes.deployThemesCss = function() {
  var style = $('<style id="fink-themes-css">' + fink.Themes.getThemesCss() + '</style>').appendTo(
    $('html > head'));
};

