/**
 * @fileoverview Footer object.
 * Create the footer.
 * @author eyal.fink@gmail.com (Eyal Fink)
 */



var fink = fink || {};

/*
 * Constructor
 */
fink.Footer = function(parentDiv) {
  this.parentDiv = parentDiv;
  this.footerDiv = $('<div id="fink-footer-top">').appendTo(
    this.parentDiv);
  $('<div class="fink-footer-block">' +
    '<div class="wrapper"><div class="cell">' +
    '<div dir="ltr">054 2406391</div>' + 
    '</div></div></div>').appendTo(this.footerDiv);
  $('<div class="fink-footer-block">' +
    '<div class="wrapper"><div class="cell">' +
    '<a href="mailto:tali@talifink.com">tali@talifink.com</a>' +
    '</div></div></div>').appendTo(this.footerDiv);
  var gplusUrl = 'https://plus.google.com/u/0/b/116100997717859550562/116100997717859550562/posts';
  var fbUrl = 'http://www.facebook.com/talifinkwellnesscoaching';
  $('<div class="fink-footer-block"><a href="' + gplusUrl +
      '"> <img class="fink-footer-icon" src="static/gplus.jpg"></a></div>').appendTo(this.footerDiv);
  $('<div class="fink-footer-block"><a href="' + fbUrl +
      '"> <img class="fink-footer-icon" src="static/facebook.jpg"></a></div>').appendTo(this.footerDiv);

};

fink.Footer.prototype.resize = function() {
};
