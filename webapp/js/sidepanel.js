/**
 * @fileoverview An object to manage the navigation side panel.
 *
 * @author eyal.fink@gmail.com (Eyal Fink)
 */



var fink = fink || {};

/*
 * Constructor
 */
fink.SidePanel = function(parentDiv, card) {
  this.parentDiv = parentDiv;

  this.topDiv = $('<div id="fink-sidepanel-top"></div>').appendTo(this.parentDiv);
  this.card = card;
  this.init();
};

fink.SidePanel.prototype.resize = function() {
  this.header.resize();
};


fink.SidePanel.prototype.init = function() {
  this.headerDiv = $('<div id="fink-header"></div>').appendTo(this.topDiv);
  this.header = new fink.Header(this.headerDiv);
  this.navDiv = $('<div id="fink-sisepanel-nav"></div>').appendTo(this.topDiv);

  for (var i in fink.consts.CARDS) {
    this.addCard(fink.consts.CARDS[i]);
  }
  this.resize();
  var currentCard = fink.App.instance.getCardAndContent();
  if (window.location.hash) {
    this.selecetCard(currentCard.info,
                     currentCard.index,
                     currentCard.selectedContent);
  } else {
    window.location.hash = currentCard.info.name;
  }
};

fink.SidePanel.prototype.addCard = function(cardInfo) {
  var navBoxDiv = $('<div class="fink-sisepanel-nav-box"></div>').appendTo(
    this.navDiv);
  navBoxDiv.attr('id', 'fink-sisepanel-nav-box-' + cardInfo.name);
  $('<div class="wrapper"><div class="cell"><div class="content"></div></div></div>').appendTo(
    navBoxDiv);
  $(navBoxDiv.find('.content')[0]).text(cardInfo.linkText);
  var that = this;
  navBoxDiv.click(function() {
    // changing the hash fires an event which triger the page change.
    window.location.hash = cardInfo.name;
    //that.selecetCard(cardInfo, this);
  });
};

fink.SidePanel.prototype.getAllThemesClasses = function() {
  if (this.themesNames) {
    return this.themesNames;
  }
  this.themesNames = [];
  var that=this;
  $(fink.consts.CARDS).each(function(i, cardInfo){
    that.themesNames.push('fink-theme-' + cardInfo.name);
  });
  return this.themesNames;
}
fink.SidePanel.prototype.selecetCard = function(cardInfo, cardIndex,
                                                contentIndex) {
  var navBoxDiv = this.navDiv.find('.fink-sisepanel-nav-box')[cardIndex]                                 ;
  // TODO: lookup from parent is faster?
  $('.fink-sisepanel-nav-box-selected').removeClass('fink-sisepanel-nav-box-selected');
  $(navBoxDiv).addClass('fink-sisepanel-nav-box-selected');
  this.card.update(cardInfo.image, cardInfo.title,
                   cardInfo.content, contentIndex);
  $('body').removeClass(this.getAllThemesClasses().join(' '));
  $('body').addClass('fink-theme-' + cardInfo.name);
};

