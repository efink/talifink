/**
 * @fileoverview An object to manage a single card on the main page.
 * A card .
 * @author eyal.fink@gmail.com (Eyal Fink)
 */



var fink = fink || {};

/*
 * Constructor
 */
fink.Card = function(cardDiv) {
  this.cardDiv = cardDiv;

  this.initDivs();
  this.carouselNav = new fink.CarouselNav(this.carouselDiv);
};

fink.Card.prototype.update = function(imageUrl, title, bodyUrl, contentIndex) {
  this.title = title;
  this.bodyUrls = typeof(bodyUrl) == "string" ? [bodyUrl] : bodyUrl;
  this.imageUrl = imageUrl;
  this.content = [];
  this.currentContentIndex = contentIndex || 0;
  this.resize();
  this.setOpened();
};

fink.Card.prototype.initDivs = function() {
  this.imageContainerDiv = $('<div id="fink-card-image-container">').appendTo(
    this.cardDiv);
  this.imageEl = $('<img id="fink-card-image">').appendTo(
    this.imageContainerDiv);
  this.titleDiv = $('<div id="fink-card-title" dir="ltr">').appendTo(
    this.imageContainerDiv);
  this.contentAndArrowDiv = $(
      '<div id="fink-card-content-and-arrow">').appendTo(this.cardDiv);
  this.carouselDiv  = $('<div id="fink-card-carousel-nav">').appendTo(
    this.contentAndArrowDiv);
  this.contentDiv = $('<div id="fink-card-content">').appendTo(
    this.contentAndArrowDiv);
};

fink.Card.prototype.resize = function() {
  if (this.imageUrl === undefined)
    return; // resize might be called before the first call to update
  var imageHeight = this.imageEl.width() * fink.consts.IMAGE_RATIO;
  var size = fink.App.viewportSize();
  if (imageHeight/size.height > 0.5) {
    this.currentImage =   this.imageUrl.replace('.jpg', '_half.jpg');
    $('#viewport').addClass('fink-small-image');
    imageHeight /=2.0;
  } else {
    $('#viewport').removeClass('fink-small-image');
    this.currentImage =   this.imageUrl;
  }
  this.imageEl.height(imageHeight);
  this.setImage();
  //var contentHeight = (size.height - this.imageEl.height());
  //this.contentAndArrowDiv.height(contentHeight);

};

fink.Card.prototype.moveNext = function() {
  if (this.currentContentIndex + 1 <  this.bodyUrls.length) {
    var index = this.currentContentIndex + 1;
    var hash = window.location.hash.substring(1);
    var parts = hash.split(':');
    this.currentContentIndex = Number(index);
    window.location.hash = parts[0] + ':' + index;
  }
  //this.setCurrentContent(this.currentContentIndex + 1);
};

fink.Card.prototype.movePrev = function() {
  if (this.currentContentIndex > 0) {
    var index = this.currentContentIndex - 1;
    var hash = window.location.hash.substring(1);
    var parts = hash.split(':');
    this.currentContentIndex = Number(index);
    window.location.hash = parts[0] + ':' + index;
  }
  //this.setCurrentContent(this.currentContentIndex - 1);
};

fink.Card.prototype.setCurrentContent = function(index) {
  if (this.content[index] === undefined) {
    this.content[index] = false; // mark as being fetched.
    var that = this;
    $.get(this.bodyUrls[index], function(data, textStatus, jqXHR) {
        that.content[index] = data;
        that.contentDiv.html(data);
        return false;
      });
  } else if (this.content[index] !== false) {
    this.contentDiv.html(this.content[index]);
  } //if this.content[index] == false than contet will be set after ajax returns
  this.currentContentIndex = index;
  this.carouselNav.update(index == 0, index == this.bodyUrls.length - 1);
};

fink.Card.prototype.setOpened = function() {
  this.setCurrentContent(this.currentContentIndex);
  this.setImage();
  this.carouselNav.replaceHandelrs($.proxy(this.movePrev, this),
                                   $.proxy(this.moveNext, this));
  //this.titleDiv.text(this.title);
};

fink.Card.prototype.setImage = function() {
  this.imageEl.attr('src', this.currentImage);
};
