/**
 * @fileoverview Header object.
 * Create the header.
 * @author eyal.fink@gmail.com (Eyal Fink)
 */



var fink = fink || {};

/*
 * Constructor
 */
fink.Header = function(parentDiv) {
  this.parentDiv = parentDiv;
  this.titleDiv = $('<div id="fink-title">').appendTo(this.parentDiv);
  this.titleDiv.append(
      '<div id="fink-logo">'+
      '<a href="/">' +
      '<img id="fink-logo-img" src="static/logo.jpg" /></a></div>');
  // this.titleDiv.append('<div id="fink-title-text-big">טלי פינק</div>');
  // this.titleDiv.append('<div id="fink-title-text-small">מאמנת לאורך חיים בריא</div>');
};

fink.Header.prototype.resize = function() {

};
