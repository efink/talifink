/**
 * @fileoverview An object to handle the nex&prev bottons
 *
 * @author eyal.fink@gmail.com (Eyal Fink)
 */



var fink = fink || {};

/*
 * Constructor
 */
fink.CarouselNav = function(parentDiv) {
  this.parentDiv = parentDiv;
  this.top = $('<div id="fink-carousel-nav-top" class="g-section g-tpl-nest">').appendTo(parentDiv);
  //this.prevArrow = $('<a id="fink-card-prev" class="right-arrow smart-hover" hover-action="spotlight" href="javascript:void(0)"><div class="arrow-icon smart-hover hidden" hover-action="fadeIn" style="opacity: 1;"></div></a>').appendTo(this.top);
  this.prevArrow = $('<a id="fink-card-prev" class="right-tri-arrow tri-arrow g-unit" ></a>').appendTo(this.top);
  //this.hr = $('<div class="g-unit fink-carousel-nav-hr">').appendTo(this.top);
  this.nextArrow = $('<a id="fink-card-next" class="left-tri-arrow tri-arrow g-unit"></a>').appendTo(this.top);
  // this.nextArrow = $('<a id="fink-card-next" class="left-arrow smart-hover" hover-action="spotlight" href="javascript:void(0)"><div class="arrow-icon smart-hover hidden" hover-action="fadeIn" style="opacity: 1;"></div></a>').appendTo(this.top);
}

fink.CarouselNav.prototype.resize = function() {
};

fink.CarouselNav.prototype.replaceHandelrs = function(prevHandler, nextHandler) {
  this.prevArrow.unbind('click');
  this.nextArrow.unbind('click');
  this.prevArrow.click(prevHandler);
  this.nextArrow.click(nextHandler);
};

fink.CarouselNav.prototype.update = function(isFirst, isLast) {
  if (isFirst && isLast) {
    this.top.hide();
  } else {
    this.top.show();
    this.prevArrow.toggle(!isFirst, 'visibility');
    this.nextArrow.toggle(!isLast, 'visibility');
  }
}
